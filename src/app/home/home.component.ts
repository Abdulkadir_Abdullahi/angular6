import { Component, OnInit } from '@angular/core';
import { Pitem } from '../shared/pitem';

@Component({

  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {


  selectedPitem: Pitem;



  onSelect(pitem: Pitem) {
    this.selectedPitem = pitem;
  }
 
  pitems: Pitem[] = [
    {
      id: '0',
      name: 'BITS',
      image: 'assets/images/1.png',
      // tslint:disable-next-line:max-line-length
      description: 'A unique combination of Indian Uthappam (pancake) and Italian pizza, topped with Cerignola olives, ripe vine cherry tomatoes, Vidalia onion, Guntur chillies and Buffalo Paneer.'
    },
    {
      id: '1',
      name: 'BITS',
      image: 'assets/images/2.png',
      description: 'Deep fried Zucchini coated with mildly spiced Chickpea flour batter accompanied with a sweet-tangy tamarind sauce'
    },
    {
      id: '2',
      name: 'IT REG SYS',
      image: 'assets/images/3.jpg',
      description: 'An IT registration system'
    },
    {
      id: '3',
      name: 'BOOTSTRAP PORTFOLIO',
      image: 'assets/images/4.png',
      description: 'A portfolio'
    },
    {
      id: '4',
      name: 'QUADRATIC',
      image: 'assets/images/5.png',
      description: 'A quadratic equition solver'
    },
    {
      id: '5',
      name: 'STORE',
      image: 'assets/images/6.png',
      description: 'A woo commerce store'
    },
    {
      id: '6',
      name: 'T ADVANCE',
      image: 'assets/images/8.jpg',
      description: 'Tranveling advance'
    },
    {
      id: '7',
      name: 'GPA',
      image: 'assets/images/9.png',
      description: 'CGPA calc'
    }
   ];

  constructor() { }

  ngOnInit() {
  }

}
