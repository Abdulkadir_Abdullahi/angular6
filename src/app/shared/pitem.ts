export class Pitem {
    id: string;
    name: string;
    image: string;
    description: string;
}